import 'package:cloud_firestore/cloud_firestore.dart';

class StoreService {
  final CollectionReference _users = FirebaseFirestore.instance.collection('users');
  final CollectionReference _sessions = FirebaseFirestore.instance.collection('sessions');

  Future<DocumentSnapshot?> getUserDocument(String uid) async {
    DocumentSnapshot? doc;
    try {
      await _users.doc(uid).get().then((result) => doc = result);
    } catch (error) {
      print(error.toString());
    }
    return doc;
  }

  createUser(uid, name, email, userType) async {
    try {
      _users.doc(uid).set({
        'name': name,
        'email': email,
        'userType': userType,
      });
    } catch (error) {
      print(error.toString());
    }
  }

  registerAttendance(sessionId, email) async {
    try {
      _sessions.doc(sessionId).update({
        'attendees': FieldValue.arrayUnion([email])
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
