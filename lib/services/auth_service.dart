import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:upasthiti/services/store_service.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final StoreService _store = StoreService();

  Stream<User?> get userStream {
    return _auth.authStateChanges();
  }

  String? get curEmail {
    return _auth.currentUser!.email;
  }

  Future<String?> getUserType(User user) async {
    String? userType;
    try {
      await _store.getUserDocument(user.uid).then((doc) => {userType = doc!['userType']});
    } catch (error) {
      print(error);
    }
    return userType;
  }

  signInWithGoogle() async {
    try {
      final GoogleSignIn _googleSignIn = GoogleSignIn();
      final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth = await googleUser!.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      User? user;
      await _auth
          .signInWithCredential(credential)
          .then((authResult) => {
                user = authResult.user!,
                if (authResult.additionalUserInfo!.isNewUser) _store.createUser(user?.uid, user?.displayName, user?.email, 'USER'),
              })
          .catchError((error) => print(error));
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  signOut() {
    _auth.signOut();
  }
}
