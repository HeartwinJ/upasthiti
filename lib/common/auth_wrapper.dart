import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upasthiti/pages/admin/admin_dashbard.dart';
import 'package:upasthiti/pages/home_page.dart';
import 'package:upasthiti/pages/user/user_dashboard.dart';
import 'package:upasthiti/services/auth_service.dart';

class AuthWrapper extends StatelessWidget {
  const AuthWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User?>(context);
    final AuthService _auth = AuthService();

    if (user != null) {
      return FutureBuilder(
        future: _auth.getUserType(user),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            print(snapshot.data);
            return snapshot.data == 'ADMIN' ? const AdminDashboard() : const UserDashboard();
          }
          return const Scaffold(
            body: Center(
              child: Icon(
                Icons.all_inclusive,
                size: 90,
              ),
            ),
          );
        },
      );
    }
    return const HomePage();
  }
}
