import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:upasthiti/pages/user/scan_page.dart';
import 'package:upasthiti/services/auth_service.dart';
import 'package:upasthiti/services/store_service.dart';

class UserDashboard extends StatelessWidget {
  const UserDashboard({Key? key}) : super(key: key);

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
  }

  _registerAttendance(qrData) async {
    StoreService _store = StoreService();
    AuthService _auth = AuthService();
    Position position = await _determinePosition();
    if (Geolocator.distanceBetween(position.latitude, position.longitude, 10.988539, 79.845788) <= 50) {
      _store.registerAttendance(qrData, _auth.curEmail);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Dashboard'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const ScanPage())).then((value) => _registerAttendance(value));
            },
            icon: const Icon(Icons.qr_code_scanner),
          ),
          IconButton(
            onPressed: () {
              AuthService _auth = AuthService();
              _auth.signOut();
            },
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: const Center(
        child: Text('Click Scan Button to scan QR!'),
      ),
    );
  }
}
