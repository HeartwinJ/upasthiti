import 'package:flutter/material.dart';

class SubjectCodeDialog extends StatelessWidget {
  SubjectCodeDialog({Key? key}) : super(key: key);
  final TextEditingController subCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Set Subject Code'),
      content: TextField(
        controller: subCodeController,
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop<String>(context, subCodeController.text);
          },
          child: const Text('Set'),
        ),
      ],
    );
  }
}
