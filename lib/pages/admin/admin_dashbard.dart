import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:upasthiti/pages/admin/subject_code_dialog.dart';
import 'package:upasthiti/services/auth_service.dart';

class AdminDashboard extends StatefulWidget {
  const AdminDashboard({Key? key}) : super(key: key);

  @override
  State<AdminDashboard> createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  String qrData = 'LigmaBallz';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Admin Dashboard'),
        actions: [
          IconButton(
            onPressed: () {
              AuthService _auth = AuthService();
              _auth.signOut();
            },
            icon: const Icon(Icons.logout),
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            qrData != 'LigmaBallz'
                ? Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    child: QrImage(
                      data: qrData,
                      version: QrVersions.auto,
                      size: 250.0,
                    ),
                  )
                : const Text('Set Subject Code!'),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  showDialog(context: context, builder: (context) => SubjectCodeDialog()).then(
                    (value) => setState(() {
                      qrData =
                          DateTime.now().add(Duration(minutes: -DateTime.now().minute, seconds: -DateTime.now().second)).toString().substring(0, 19) +
                              value;
                    }),
                  );
                },
                child: const Text('Set Subject Code'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
