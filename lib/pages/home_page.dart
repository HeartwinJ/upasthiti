import 'package:flutter/material.dart';
import 'package:upasthiti/services/auth_service.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const Expanded(
                child: Icon(
                  Icons.all_inclusive,
                  size: 90,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: ElevatedButton(
                  onPressed: () {
                    AuthService _auth = AuthService();
                    _auth.signInWithGoogle();
                  },
                  child: const Text('Sign In with Google'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
